/*
Requires area, defaultDistrict, and defaultState to be set before loading this script.
*/

/**
 * @param mixed selector E.g. "#state_id"
 * @param JSONObject KV pairs For generating <option> tags.
 * @param string selected Default selected option value.
 */
function populateSelector(selector, options, selected) {
	selected = selected || '';

	var optionHTML = '<option value=""></option>';
	for (var key in options) {
		if (selected == key) {
			optionHTML += '<option value="'+key+'" selected="selected">'+options[key]+'</option>';
		}
		else {
			optionHTML += '<option value="'+key+'">'+options[key]+'</option>';
		}
	}

	// Sort and replace HTML in <select>
	$(selector).html($(optionHTML).sort(function(a, b){
		return (a.innerHTML > b.innerHTML) ? 1 : -1;
	}));
}

$(function() {
	$('#date').datepicker({
		dateFormat: 'yy-mm-dd',
	});

	// Populate states
	var $state = $('#state_id');
	var optionHTML = '<option value=""></option>';
	for (var stateId in areas) {
		optionHTML += '<option value="'+stateId+'">'+areas[stateId]['name']+'</option>';
	}

	$state.html($(optionHTML).sort(function(a, b){
		return (a.innerHTML > b.innerHTML) ? 1 : -1;
	}));

	// Change districts accordingly
	$state.change(function(event){
		if ($(this).val() == '') {
			$('#district_id').html('');
		}
		else {
			populateSelector('#district_id', areas[$(this).val()]['districts']);
		}
	});

	// Select initial state and district
	if (defaultState != '') {
		$state.val(defaultState);
		populateSelector('#district_id', areas[$state.val()]['districts'], defaultDistrict);
	}
});