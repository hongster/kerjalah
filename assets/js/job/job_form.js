/*
Requires areas, defaultDistrict, defaultState, defaultStarTime, defaultEndTime
to be set before loading this script.
*/

/**
 * @param mixed selector E.g. "#state_id"
 * @param JSONObject KV pairs For generating <option> tags.
 * @param string selected Default selected option value.
 */
function populateSelector(selector, options, selected) {
	selected = selected || '';

	var optionHTML = '';
	for (var key in options) {
		if (selected == key) {
			optionHTML += '<option value="'+key+'" selected="selected">'+options[key]+'</option>';
		}
		else {
			optionHTML += '<option value="'+key+'">'+options[key]+'</option>';
		}
	}

	// Sort and replace HTML in <select>
	$(selector).html($(optionHTML).sort(function(a, b){
		return (a.innerHTML > b.innerHTML) ? 1 : -1;
	}));
}

$(function(){
	$('#start_date').datepicker({
		dateFormat: 'yy-mm-dd',
	});

	// For updating existing job post, need to pre-populate duration field.
	var startDate = $('#start_date').val();
	var endDate = $('#end_date').val();
	
	if ((startDate != '') && (endDate != '')) {
		startDate = Date.parse(startDate);
		endDate = Date.parse(endDate);
		duration = (endDate - startDate) / 86400000 + 1;
		$('#duration').val(duration);
	}

	// Update hidden #end_date field
	$('#start_date, #duration').change(function(event){
		var startDate = $('#start_date').val();
		if (startDate == '')
			return;

		var duration = $('#duration').val();
		var startTime = Date.parse(startDate);
		//86400000 = 24h * 60m * 60s * 1000ms
		startTime += (duration - 1) * 86400000;

		endDate = new Date();
		endDate.setTime(startTime);
		$('#end_date').val($.datepicker.formatDate('yy-mm-dd', endDate));
	});

	var optionHTML = '<option value=""></option>';

	// Populate start_date and end_date
	// 24 hours, 30min interval
	for (var hour=0;hour<24;hour++) {
		for (var minute=0;minute<2;minute++) {
			value = (hour < 10) ? '0' + hour.toString() : hour.toString();
			value += ':' + ((minute == 0) ? '00' : '30');
			optionHTML += '<option value="'+value+'">'+value+'</option>';
		}
	}
	$("#start_time, #end_time").html(optionHTML);
	$("#start_time").val(defaultStarTime);
	$("#end_time").val(defaultEndTime);

	// Populate states
	var $state = $('#state_id');
	optionHTML = '';
	for (var stateId in areas) {
		optionHTML += '<option value="'+stateId+'">'+areas[stateId]['name']+'</option>';
	}

	$state.html($(optionHTML).sort(function(a, b){
		return (a.innerHTML > b.innerHTML) ? 1 : -1;
	}));

	// Change districts accordingly
	$state.change(function(event){
		populateSelector('#district_id', areas[$(this).val()]['districts']);
	});

	// Select initial state and district
	$state.val(defaultState);
	populateSelector('#district_id', areas[$state.val()]['districts'], defaultDistrict);
});