/*
Requires tokenUrl to be set.
*/
(function() {
	if (typeof window.janrain !== 'object') window.janrain = {};
	if (typeof window.janrain.settings !== 'object') window.janrain.settings = {};

	// XXX Make the tokenUrl is set http://bit.ly/Ta0zqF
	janrain.settings.tokenUrl = tokenUrl;
	janrain.settings.tokenAction = 'event';

	function isReady() { janrain.ready = true; };
	if (document.addEventListener) {
		document.addEventListener("DOMContentLoaded", isReady, false);
	} 
	else {
		window.attachEvent('onload', isReady);
	}

	var e = document.createElement('script');
	e.type = 'text/javascript';
	e.id = 'janrainAuthWidget';

	if (document.location.protocol === 'https:') {
		e.src = 'https://rpxnow.com/js/lib/kerjalah/engage.js';
	} 
	else {
		e.src = 'http://widget-cdn.rpxnow.com/js/lib/kerjalah/engage.js';
	}

	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(e, s);
})();
function janrainWidgetOnload() {
	janrain.events.onProviderLoginToken.addHandler(function(tokenResponse) {
		$.ajax({
			type: 'POST',
			url: tokenUrl,
			data: 'token=' + tokenResponse.token,
			success: function(res){
				if (res.error) {
					$('#lead-message').html('There seems to be a problem with the sign in process. If problem persists, please contact hello@kerjalah.cc.');
					janrain.engage.signin.widget.refresh();
					return;
				}

				location.href = res.redirectUrl;
			}
		});
	});

	janrain.events.onReturnExperienceFound.addHandler(function(response) {
		$('#lead-message').html('Welcome back, ' + response.welcomeName + '. Login to manage your job postings.');
	});

	janrain.events.onProviderLoginError.addHandler(function(response) {
		$('#lead-message').html('Login was not completed successfully. Please try again.');
		janrain.engage.signin.widget.refresh();
	});
}