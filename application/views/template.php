<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="utf-8" />
	<title><?php echo isset($title) ? HTML::chars($title) : 'Part Time Job Listing In Malaysia'; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="<?php echo isset($description) ? HTML::chars($description) : 'Part Time Jobs In Malaysia'; ?>" />
	<meta name="author" content="Kerjalah hello@kerjalah.cc" />
	<meta property="og:site_name" content="Kerjalah" />
	<meta property="og:title" content="<?php echo isset($title) ? HTML::chars($title) : 'Part Time Job Listing In Malaysia'; ?>" />
	<meta property="og:description" content="<?php echo isset($description) ? HTML::chars($description) : 'Part Time Jobs In Malaysia'; ?>" />
	<meta property="og:url" content="<?php echo isset($canonical_url) ? $canonical_url : Request::current()->url(TRUE).URL::query(); ?>" />
	<meta property="og:img" content="<?php echo URL::site('assets/img/kerjalah_logo_64x64.png', TRUE); ?>" />
	
	<link rel="icon" type="image/x-icon" href="<?php echo URL::site('favicon.ico'); ?>" />

	<?php echo HTML::style('assets/css/bootstrap.css'); ?>
	<?php echo HTML::style('assets/css/bootstrap-responsive.css'); ?>
	<?php echo HTML::style('assets/css/start/jquery-ui-1.9.2.custom.min.css'); ?>
	<?php echo HTML::style('assets/css/style.css'); ?>
	
	<?php echo HTML::script('assets/js/jquery-1.8.3.min.js'); ?>

	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-37276812-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>
</head>
<body>
	<header class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<?php echo HTML::anchor(
					'',
					HTML::image('assets/img/kerjalah_logo_64x64.png', array('id' => 'logo')).'Kerjalah',
					array('class' => 'brand'));
				?>
				
				<div class="nav-collapse collapse">
					<ul class="nav pull-right">
						<li class="active"><?php echo Html::anchor('job/post', 'Post A Job'); ?></li>
						<li><?php echo View::factory('Template/login_button'); ?></li>
					</ul>
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</div><!-- /.navbar-inner -->
	</header><!-- /.navbar -->

	<div id="container" class="container">
		<?php echo View::factory('Template/flash_message'); ?>

		<?php if (isset($content)) echo $content; ?>
	</div><!-- /#container -->

	<footer class="muted">
		<div class="container">
			<a href="http://creativecommons.org/licenses/by/3.0/" rel="license">(CC) BY</a>
			&sdot; Contact: <?php echo HTML::mailto('hello@kerjalah.cc')?>
		</div>
	</footer>

	<?php echo HTML::script('assets/js/bootstrap.min.js'); ?>
	<?php echo HTML::script('assets/js/jquery-ui-1.9.2.custom.min.js'); ?>
	<?php echo HTML::script('assets/js/uservoice.js'); ?>
</body>
</html>