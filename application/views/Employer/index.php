<h1>My Job Posts</h1>

<?php if (count($jobs) == 0): ?>
	<p>You have not posted any job yet. <?php echo HTML::anchor('job/post', 'Post A Job', array('class' => 'btn btn-primary')); ?> now.</p>
<?php else: ?>
	<table class="table table-hover">
		<thead>
			<th>Title</th>
			<th>Work Date</th>
			<th>Salary</th>
		</thead>

		<tbody>
			<?php foreach ($jobs as $job): ?>
				<tr>
					<td><?php echo HTML::anchor(Template::job_url($job), HTML::chars($job->title)); ?></td>
					<td>
						<?php echo $job->start_date; ?>
						<?php echo ($job->start_date == $job->end_date) ? '' : "- {$job->end_date}"; ?>
					</td>
					<td>
						<?php echo $job->salary; ?>
						<?php echo ($job->salary_unit == 1) ? '/ hour' : '/ day'; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php echo $paginate->render(); ?>
<?php endif; ?>