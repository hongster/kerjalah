<div class="row">
	<div class="span3"></div>

	<div class="span6">
		<h1>Login To Post Job</h1>

		<p class="lead" id="lead-message">No registration is required. Just sign-in with your Google/Facebook account,
			and you will be able to start posting new job.
		</p>

		<div id="janrainEngageEmbed"></div>
	</div>
	
	<div class="span3"></div>
</div>

<script type="text/javascript">
	var tokenUrl = '<?php echo URL::site('auth/index', TRUE); ?>';
</script>
<?php echo HTML::script('assets/js/auth/signin-widget.js'); ?>