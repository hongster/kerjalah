<?php echo My_Form::open(null, array('class' => 'form-horizontal')); ?>
	<?php echo My_Form::hidden('end_date', $values); ?>

	<div class="control-group">
		<?php echo My_Form::label('title', 'Title*', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::input('title', $values, array('class' => 'input-xxlarge', 'placeholder' => 'Descriptive title')); ?>
			<?php echo My_Form::error('title', $errors); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('description', 'Description*', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::textarea('description', $values, array('class' => 'input-xxlarge', 'placeholder' => 'Nature of work, and job requirements')); ?>
			<?php echo My_Form::error('description', $errors); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('company', 'Company', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::input('company', $values, array('class' => 'input-xxlarge', 'placeholder' => 'ABC Sdn Bhd')); ?>
			<?php echo My_Form::error('company', $errors); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('address', 'Address*', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::input('address', $values, array('class' => 'input-xxlarge', 'placeholder' => 'Location of workplace')); ?>
			<?php echo My_Form::error('address', $errors); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('state_id', 'State', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::select('state_id', array(), $values); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('district_id', 'District', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::select('district_id', array(), $values); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('salary', 'Salary*', array('class' => 'control-label')); ?>
		<div class="controls input-prepend" style="margin-left:20px;">
			<span class="add-on">RM</span>
			<?php echo My_Form::input('salary', $values, array('class' => 'input-mini')); ?>

			<?php echo My_Form::select('salary_unit', array(1 => 'per hour', 24 => 'per day'), $values); ?>
		</div>
		<?php echo My_Form::error('salary', $errors); ?>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('start_date', 'Start Date*', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::input('start_date', $values, array('readonly' => 'readonly')); ?>
			<?php echo My_Form::error('start_date', $errors); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('duration', 'Duration', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php echo My_Form::select('duration', Arr::range(1, 9), $values); ?> days
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('start_time', 'Start Time', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php $values['start_time'] = isset($values['start_time']) ? substr($values['start_time'], 0, 5) : NULL; ?>
			<?php echo My_Form::select('start_time', array(), $values); ?>
		</div>
	</div>

	<div class="control-group">
		<?php echo My_Form::label('end_time', 'End Time', array('class' => 'control-label')); ?>
		<div class="controls">
			<?php $values['end_time'] = isset($values['end_time']) ? substr($values['end_time'], 0, 5) : NULL; ?>
			<?php echo My_Form::select('end_time', array(), $values); ?>
			<?php echo My_Form::error('end_time', $errors); ?>
		</div>
	</div>

	<div class="form-actions">
		<?php echo My_Form::submit('', 'Save', array('class' => 'btn btn-primary')); ?>
		<?php echo isset($cancel_url) ? HTML::anchor($cancel_url, 'Cancel', array('class' => 'btn')) : ''; ?>
	</div>
<?php echo My_Form::close(); ?>
