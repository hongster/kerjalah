<div class="page-header">
	<h1><?php echo HTML::chars($job->title); ?></h1>
	<?php if ($job->company): ?>
		from <?php echo HTML::anchor(
			Route::url('jobs_by_company', array('company' => rawurlencode($job->company)), TRUE),
			$job->company
		); 
	?>
	<?php endif; ?>
</div>

<h3>Description</h3>

<div class="well">
	<?php echo Text::auto_p(Text::auto_link(HTML::chars($job->description))); ?>
</div>

<?php echo View::factory('Job/_share_widget')->render(); ?>

<h3>Additional Information</h3>

<table class="table table-striped">
	<?php if (isset($job->company)): ?>
		<tr>
			<th>Company</th>
			<td><?php echo HTML::chars($job->company); ?></td>
		</tr>
	<?php endif; ?>

	<tr>
		<th>Area</th>
		<td><?php echo $job->district->name.", ".$job->district->state->name; ?></td>
	</tr>

	<tr>
		<th>Address</th>
		<td><address><?php echo HTML::chars($job->address); ?></address></td>
	</tr>

	<tr>
		<th>Salary</th>
		<td>
			RM <?php echo $job->salary; ?>
			<?php echo ($job->salary_unit == 1) ? '/ hour' : '/ day'; ?>
		</td>
	</tr>

	
	<tr>
		<th>Date</th>
		<td>
			<?php echo $job->start_date; ?>
			<?php echo ($job->end_date != $job->start_date) ? ' to '.$job->end_date : ''; ?>
		</td>
	</tr>

	<?php if (isset($job->start_time) || isset($job->end_time)): ?>
		<tr>
			<th>Time</th>
			<td>
				<?php if (isset($job->start_time)) echo 'Starts from: '.substr($job->start_time, 0, 5).'h'; ?>
				<?php if (isset($job->start_time) && isset($job->end_time)) echo '<br />'; ?>
				<?php if (isset($job->end_time)) echo 'Ends at: '.substr($job->end_time, 0, 5).'h'; ?>
			</td>
		</tr>
	<?php endif; ?>
</table>

<?php if (Authen::instance()->logged_in() && ($job->user->id == Authen::instance()->user()->id)): ?>
	<div class="form-action">
		<?php echo HTML::anchor('job/update/'.$job->id, 'Update', array('class' => 'btn btn-primary', 'rel' => 'tooltip', 'title' => 'Make changes to this job post.')); ?>
		<?php echo HTML::anchor('job/post/'.$job->id, 'Copy Job Post', array('class' => 'btn', 'rel' => 'tooltip', 'title' => 'Create new job post with similar information.')); ?>
		<?php echo HTML::anchor('job/delete/'.$job->id, 'Delete', array('id' => 'delete', 'class' => 'muted pull-right')); ?>
	</div>

	<script type="text/javascript">
		$(function() {
			$('#delete').click(function(event){
				var conf = confirm('Once a job post is deleted, it is not recoverable. Are you sure you want to delete this job post?');
				
				if (conf) {
					return true;
				}
				else {
					event.preventDefault();
				}
			});

			$('.btn').tooltip();
		})
	</script>
<?php endif; ?>

<?php if (count($related_jobs)): ?>
	<h3>Related Jobs</h3>

	<ul>
		<?php foreach ($related_jobs as $related_job): ?>
			<li><?php echo HTML::anchor(Template::job_url($related_job), $related_job->title); ?></li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>