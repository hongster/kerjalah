<p class="alert alert-info">Kerjalah is designed to make searching for part time jobs in Malaysia easy.
	Specify your state and district in the search form. Contact the employer,
	start earning!
</p>

<div class="row-fluid">
	<span class="span3">
		<h2>Search</h2>
		<?php echo My_Form::open(null, array('method' => 'GET')); ?>
			<?php echo My_Form::label('title', 'Job'); ?>
			<?php echo My_Form::input('title', $values, array('placeholder' => 'E.g. promoter')); ?>

			<?php echo My_Form::label('state_id', 'State'); ?>
			<?php echo My_Form::select('state_id', array(), $values); ?>

			<?php echo My_Form::label('district_id', 'District'); ?>
			<?php echo My_Form::select('district_id', array(), $values); ?>

			<?php echo My_Form::label('date', 'Date'); ?>
			<?php echo My_Form::input('date', $values); ?>

			<?php echo My_Form::label('company', 'Company'); ?>
			<?php echo My_Form::input('company', $values); ?>

			<?php echo My_Form::submit('', 'Submit', array('class' => 'btn btn-primary')); ?>
		<?php echo My_Form::close(); ?>

		<h2>Jobs In State</h2>
		
		<ul class="nav nav-stacked">
			<li><?php echo HTML::anchor('/?state_id=4', 'Johor'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=5', 'Kedah'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=6', 'Kelantan'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=1', 'Kuala Lumpur'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=15', 'Labuan'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=7', 'Melaka'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=8', 'Negeri Sembilan'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=9', 'Pahang'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=3', 'Penang'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=10', 'Perak'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=11', 'Perlis'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=16', 'Putrajaya'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=13', 'Sabah'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=14', 'Sarawak'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=2', 'Selangor'); ?></li>
			<li><?php echo HTML::anchor('/?state_id=12', 'Terengganu'); ?></li>
		</ul>
	</span><!-- /left sidebar -->

	<span class="span9">
		<h2><?php echo HTML::chars($title); ?></h2>

		<?php if (count($jobs)): ?>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Working Date</th>
						<th>Area</th>
						<th>Salary</th>
						<th>Job</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach ($jobs as $job): ?>
						<tr>
							<td><?php echo $job->start_date; ?></td>
							<td><?php echo $job->district->name.', '.$job->district->state->name; ?></td>
							<td>RM <?php echo $job->salary ?> / <?php echo ($job->salary_unit == 1) ? 'hour' : 'day'; ?></td>
							<td>
								<?php echo HTML::anchor(Template::job_url($job), HTML::chars($job->title)); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php else: ?>
			<p class="alert">There is no job found for your search criteria.</p>
		<?php endif; ?>

		<?php echo $paginate->render(); ?>
	</span><!-- /search results -->
</div>

<script type="text/javascript">
	var areas = <?php echo Request::factory('info/areas')->execute(); ?>;
	var defaultDistrict = '<?php echo Arr::get($values, 'district_id', ''); ?>';
	var defaultState = '<?php echo Arr::get($values, 'state_id', ''); ?>';
</script>

<?php echo HTML::script('assets/js/job/index.js'); ?>