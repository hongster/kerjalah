<h1>Upcoming Jobs From <?php echo HTML::chars($company); ?></h1>

<?php if (count($jobs) > 0): ?>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Working Date</th>
				<th>Area</th>
				<th>Salary</th>
				<th>Job</th>
			</tr>
		</thead>

		<tbody>
			<?php foreach ($jobs as $job): ?>
				<tr>
					<td><?php echo $job->start_date; ?></td>
					<td><?php echo $job->district->name.', '.$job->district->state->name; ?></td>
					<td>RM <?php echo $job->salary ?> / <?php echo ($job->salary_unit == 1) ? 'hour' : 'day'; ?></td>
					<td>
						<?php echo HTML::anchor(Template::job_url($job), HTML::chars($job->title)); ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<p class="alert">There is no upcoming job from this company.</p>
<?php endif; ?>

<div class="row">
	<div class="span12">
		<?php echo $paginate->render(); ?>
	</div>
</div>