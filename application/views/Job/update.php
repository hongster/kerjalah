<h1>Update Job Post</h1>

<?php include Kohana::find_file('views', 'Job/_job_form'); ?>

<script type="text/javascript">
	var areas = <?php echo Request::factory('info/areas')->execute(); ?>;
	var defaultDistrict = '<?php echo Arr::get($values, 'district_id', ''); ?>';
	var defaultState = '<?php echo Arr::get($values, 'state_id', ''); ?>';
	var defaultStarTime = '<?php echo Arr::get($values, 'start_time', ''); ?>';
	var defaultEndTime = '<?php echo Arr::get($values, 'end_time', ''); ?>';	
</script>

<?php echo HTML::script('assets/js/job/job_form.js'); ?>