<?php if ($message = Session::instance()->get_once(Controller_Web::ERROR_MESSAGE, false)): ?>
	<div class="alert alert-error fade">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $message; ?>
	</div>
<?php endif; ?>

<?php if ($message = Session::instance()->get_once(Controller_Web::SUCCESS_MESSAGE, false)): ?>
	<div class="alert alert-success fade">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $message; ?>
	</div>
<?php endif; ?>

<?php if ($message = Session::instance()->get_once(Controller_Web::INFO_MESSAGE, false)): ?>
	<div class="alert alert-info fade">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<?php echo $message; ?>
	</div>
<?php endif; ?>

<?php if (isset($message)): ?>
	<script type="text/javascript">
		$(function(){
			$(".alert").addClass("in");
		});
	</script>
<?php endif; ?>