<?php if (Authen::instance()->logged_in()): ?>
	<?php echo HTML::anchor(Route::get('logout')->uri(), 'Logout'); ?>
<?php else: ?>
	<?php echo HTML::anchor(Route::get('login')->uri(), 'Employer Login'); ?>
<?php endif; ?>