<div class="pagination">
	<ul>
		<?php if ($paginate->current_page() == 1): ?>
			<li class="disabled"><a href="#">&laquo;</a></li>
		<?php else: ?>
			<li><?php echo HTML::anchor($paginate->page_url(1), '&laquo;'); ?></li>
		<?php endif; ?>

		<?php for ($i=$paginate->start_page();$i<=$paginate->end_page();$i++): ?>
			<?php if ($paginate->current_page() == $i): ?>
				<li class="active"><?php echo HTML::anchor('#', $i); ?></li>
			<?php else: ?>
				<li><?php echo HTML::anchor($paginate->page_url($i), $i); ?></li>
			<?php endif; ?>
		<?php endfor; ?>

		<?php if ($paginate->current_page() >= $paginate->page_count()): // page_count may be 0 ?>
			<li class="disabled"><a href="#">&raquo;</a></li>
		<?php else: ?>
			<li><?php echo HTML::anchor($paginate->page_url($paginate->page_count()), '&raquo;'); ?></li>
		<?php endif; ?>
	</ul>
</div>