<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	'native' => array(
		'name' => 'kerjalah_session',
		'lifetime' => 0,
	),
	'cookie' => array(
		'name' => 'kerjalah_session',
		'encrypted' => FALSE,
	),
);