-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `kerjalah`;

CREATE TABLE `authentications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `provider` varchar(100) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `open_id` (`identifier`),
  CONSTRAINT `authentications_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `districts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `districts_ibfk_2` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `districts` (`id`, `state_id`, `name`) VALUES
(1,	1,	'Ampang'),
(2,	1,	'Bandar Menjalara'),
(3,	1,	'Bandar Tasik Selatan'),
(4,	1,	'Bandar Tun Razak'),
(5,	1,	'Bangsar'),
(6,	1,	'Bangsar South'),
(7,	1,	'Batu'),
(8,	1,	'Brickfields'),
(9,	1,	'Bukit Bintang'),
(10,	1,	'Bukit Jalil'),
(11,	1,	'Bukit Ledang'),
(12,	1,	'Cheras'),
(13,	1,	'Country Heights'),
(14,	1,	'Damansara'),
(15,	1,	'Damansara Heights'),
(16,	1,	'Desa Pandan'),
(17,	1,	'Desa ParkCity'),
(18,	1,	'Desa Petaling'),
(19,	1,	'Gombak'),
(20,	1,	'Jalan Ipoh'),
(21,	1,	'Jalan Klang Lama (Old Klang Road)'),
(22,	1,	'Jalan Kuching'),
(23,	1,	'Jalan Sultan Ismail'),
(24,	1,	'Jinjang'),
(25,	1,	'Kenny Hills (Bukit Tunku)'),
(26,	1,	'Kepong'),
(27,	1,	'Keramat'),
(28,	1,	'KL City'),
(29,	1,	'KL Sentral'),
(30,	1,	'KLCC'),
(31,	1,	'Kuchai Lama'),
(32,	1,	'Lembah Pantai'),
(33,	1,	'Mid Valley City'),
(34,	1,	'Mont Kiara'),
(35,	1,	'Taman OUG'),
(36,	1,	'Pandan Indah'),
(37,	1,	'Pandan Jaya'),
(38,	1,	'Pantai'),
(39,	1,	'Pekan Batu'),
(40,	1,	'Puchong'),
(41,	1,	'Pudu'),
(42,	1,	'Salak Selatan'),
(43,	1,	'Segambut'),
(44,	1,	'Sentul'),
(45,	1,	'Seputeh'),
(46,	1,	'Serdang'),
(47,	1,	'Setapak'),
(48,	1,	'Setiawangsa'),
(49,	1,	'Solaris Dutamas'),
(50,	1,	'Sri Damansara'),
(51,	1,	'Sri Hartamas'),
(52,	1,	'Sri Petaling'),
(53,	1,	'Sungai Besi'),
(54,	1,	'Sungai Penchala'),
(55,	1,	'Taman Desa'),
(56,	1,	'Taman Duta'),
(57,	1,	'Taman Melawati'),
(58,	1,	'Taman Tun Dr Ismail'),
(59,	1,	'Technology Park Malaysia'),
(60,	1,	'Titiwangsa'),
(61,	1,	'Wangsa Maju'),
(62,	1,	'Bukit Keramat'),
(63,	1,	'Bukit Prima Pelangi'),
(64,	1,	'Segambut Tengah'),
(65,	1,	'Pandan Perdana'),
(66,	1,	'Taman Shamelin'),
(67,	1,	'Ampang Hilir'),
(68,	1,	'Kampung Pandan'),
(69,	1,	'Taman U-Thant'),
(70,	1,	'Kerinchi'),
(71,	1,	'Pantai Dalam'),
(72,	1,	'Sunway Spk Damansara'),
(73,	2,	'Ampang'),
(74,	2,	'Ara Damansara'),
(75,	2,	'Balakong'),
(76,	2,	'Bandar Bukit Raja'),
(77,	2,	'Bandar Kinrara'),
(78,	2,	'Bandar Puteri'),
(79,	2,	'Bandar Sunway'),
(80,	2,	'Bandar Utama'),
(81,	2,	'Bangi'),
(82,	2,	'Banting'),
(83,	2,	'Batang Berjuntai'),
(84,	2,	'Batang Kali'),
(85,	2,	'Batu Arang'),
(86,	2,	'Batu Caves'),
(87,	2,	'Beranang'),
(88,	2,	'Bukit Rotan'),
(89,	2,	'Cheras'),
(90,	2,	'Country Heights'),
(91,	2,	'Cyberjaya'),
(92,	2,	'Damansara Damai'),
(93,	2,	'Damansara Intan'),
(94,	2,	'Damansara Jaya'),
(95,	2,	'Damansara Kim'),
(96,	2,	'Damansara Perdana'),
(97,	2,	'Damansara Utama'),
(98,	2,	'Dengkil'),
(99,	2,	'Glenmarie'),
(100,	2,	'Gombak'),
(101,	2,	'Hulu Langat'),
(102,	2,	'Hulu Selangor'),
(103,	2,	'Jenjarom'),
(104,	2,	'Kajang'),
(105,	2,	'Kapar'),
(106,	2,	'Kayu Ara'),
(107,	2,	'Kelana Jaya'),
(108,	2,	'Kerling'),
(109,	2,	'Klang'),
(110,	2,	'Kota Damansara'),
(111,	2,	'Kota Kemuning'),
(112,	2,	'Kuala Kubu Baru'),
(113,	2,	'Kuala Langat'),
(114,	2,	'Kuala Selangor'),
(115,	2,	'Kuang'),
(116,	2,	'Mutiara Damansara'),
(117,	2,	'Petaling Jaya'),
(118,	2,	'Port Klang'),
(119,	2,	'Puchong'),
(120,	2,	'Pulau Indah (Pulau Lumut)'),
(121,	2,	'Pulau Carey'),
(122,	2,	'Pulau Ketam'),
(123,	2,	'Puncak Jalil'),
(124,	2,	'Putra Heights'),
(125,	2,	'Putrajaya'),
(126,	2,	'Rasa'),
(127,	2,	'Rawang'),
(128,	2,	'Sabak Bernam'),
(129,	2,	'Sekinchan'),
(130,	2,	'Selayang'),
(131,	2,	'Semenyih'),
(132,	2,	'Sepang'),
(133,	2,	'Serdang'),
(134,	2,	'Serendah'),
(135,	2,	'Seri Kembangan'),
(136,	2,	'Setia Alam'),
(137,	2,	'Shah Alam'),
(138,	2,	'SierraMas'),
(139,	2,	'SS2'),
(140,	2,	'Subang Jaya'),
(141,	2,	'Sungai Ayer Tawar'),
(142,	2,	'Sungai Besar'),
(143,	2,	'Sungai Buloh'),
(144,	2,	'Sungai Pelek'),
(145,	2,	'Tanjong Karang'),
(146,	2,	'Tanjong Sepat'),
(147,	2,	'Telok Panglima Garang'),
(148,	2,	'Tropicana'),
(149,	2,	'Ulu Klang'),
(150,	2,	'USJ'),
(151,	2,	'Valencia'),
(152,	2,	'Setia Eco Park'),
(153,	2,	'Bukit Jelutong'),
(154,	2,	'Subang Heights'),
(155,	2,	'Bandar Putra Permai'),
(156,	2,	'Batu 9th Cheras'),
(157,	2,	'Hulu Kelang'),
(158,	2,	'Jeram'),
(159,	2,	'Subang'),
(160,	2,	'Ampang Jaya'),
(161,	2,	'Bandar Sungai Long'),
(162,	2,	'Damansara Uptown'),
(163,	2,	'Taman Sea'),
(164,	2,	'Taman Megah'),
(165,	2,	'Taman Megah Mas'),
(166,	2,	'Taman Mayang'),
(167,	2,	'Saujana'),
(168,	2,	'Bandar Country Homes'),
(169,	2,	'Bukit Subang'),
(170,	2,	'USJ Heights'),
(171,	3,	'Air Tawar'),
(172,	3,	'Alma'),
(173,	3,	'Ayer Itam'),
(174,	3,	'Bagan Ajam'),
(175,	3,	'Bagan Jermal'),
(176,	3,	'Bagan Lalang'),
(177,	3,	'Balik Pulau'),
(178,	3,	'Bandar Perda'),
(179,	3,	'Barat Daya'),
(180,	3,	'Batu Ferringhi'),
(181,	3,	'Batu Kawan'),
(182,	3,	'Batu Maung'),
(183,	3,	'Batu Uban'),
(184,	3,	'Bayan Baru'),
(185,	3,	'Bayan Lepas'),
(186,	3,	'Berapit'),
(187,	3,	'Bertam'),
(188,	3,	'Bukit Dumbar'),
(189,	3,	'Bukit Hambul'),
(190,	3,	'Bukit Jambul'),
(191,	3,	'Bukit Mertajam'),
(192,	3,	'Bukit Minyak'),
(193,	3,	'Bukit Tambun'),
(194,	3,	'Bukit Tengah'),
(195,	3,	'Butterworth'),
(196,	3,	'Gelugor'),
(197,	3,	'Georgetown'),
(198,	3,	'Gertak Sanggul'),
(199,	3,	'Greenlane'),
(200,	3,	'Jawi'),
(201,	3,	'Jelutong'),
(202,	3,	'Juru'),
(203,	3,	'Kepala Batas'),
(204,	3,	'Kubang Semang'),
(205,	3,	'Mak Mandin'),
(206,	3,	'Minden Heights'),
(207,	3,	'Nibong Tebal'),
(208,	3,	'Pauh Jaya'),
(209,	3,	'Paya Terubong'),
(210,	3,	'Penaga'),
(211,	3,	'Penang Hill'),
(212,	3,	'Penanti'),
(213,	3,	'Perai'),
(214,	3,	'Permatang Kuching'),
(215,	3,	'Permatang Pauh'),
(216,	3,	'Permatang Tinggi'),
(217,	3,	'Persiaran Gurney'),
(218,	3,	'Prai'),
(219,	3,	'Pulau Betong'),
(220,	3,	'Pulau Pinang'),
(221,	3,	'Pulau Tikus'),
(222,	3,	'Raja Uda'),
(223,	3,	'Relau'),
(224,	3,	'Scotland'),
(225,	3,	'Seberang Jaya'),
(226,	3,	'Seberang Perai'),
(227,	3,	'Serdang'),
(228,	3,	'Simpang Ampat'),
(229,	3,	'Sungai Ara'),
(230,	3,	'Sungai Bakap'),
(231,	3,	'Sungai Dua (Island)'),
(232,	3,	'Sungai Jawi'),
(233,	3,	'Sungai Nibong'),
(234,	3,	'Sungai Pinang'),
(235,	3,	'Tanjung Tokong'),
(236,	3,	'Tanjung Bungah'),
(237,	3,	'Tasek Gelugor'),
(238,	3,	'Teluk Bahang'),
(239,	3,	'Teluk Kumbar'),
(240,	3,	'Timur Laut'),
(241,	3,	'USM'),
(242,	3,	'Valdor'),
(243,	3,	'Sungai Dua (Mainland)'),
(244,	3,	'Kampung Gajah'),
(245,	3,	'Cangkat'),
(246,	3,	'Sungai Puyu'),
(247,	4,	'Ayer Baloi'),
(248,	4,	'Ayer Hitam'),
(249,	4,	'Bakri'),
(250,	4,	'Batu Anam'),
(251,	4,	'Batu Pahat'),
(252,	4,	'Bekok'),
(253,	4,	'Benut'),
(254,	4,	'Bukit Gambir'),
(255,	4,	'Bukit Pasir'),
(256,	4,	'Chaah'),
(257,	4,	'Endau'),
(258,	4,	'Gelang Patah'),
(259,	4,	'Gerisek'),
(260,	4,	'Gugusan Taib Andak'),
(261,	4,	'Jementah'),
(262,	4,	'Johor Bahru'),
(263,	4,	'Kahang'),
(264,	4,	'Kampung Kenangan Tun Dr Ismail'),
(265,	4,	'Kluang'),
(266,	4,	'Kota Tinggi'),
(267,	4,	'Kukup'),
(268,	4,	'Kulai'),
(269,	4,	'Labis'),
(270,	4,	'Layang Layang'),
(271,	4,	'Ledang'),
(272,	4,	'Masai'),
(273,	4,	'Mersing'),
(274,	4,	'Muar'),
(275,	4,	'Pagoh'),
(276,	4,	'Paloh'),
(277,	4,	'Panchor'),
(278,	4,	'Parit Jawa'),
(279,	4,	'Parit Raja'),
(280,	4,	'Parit Sulong'),
(281,	4,	'Pasir Gudang'),
(282,	4,	'Pekan Nanas (Pekan Nenas)'),
(283,	4,	'Pengerang'),
(284,	4,	'Permas Jaya'),
(285,	4,	'Plentong'),
(286,	4,	'Pontian'),
(287,	4,	'Rengam'),
(288,	4,	'Rengit'),
(289,	4,	'Segamat'),
(290,	4,	'Semerah'),
(291,	4,	'Senai'),
(292,	4,	'Senggarang'),
(293,	4,	'Seri Gading'),
(294,	4,	'Simpang Renggam'),
(295,	4,	'Skudai'),
(296,	4,	'Sungai Mati'),
(297,	4,	'Tampoi'),
(298,	4,	'Tangkak'),
(299,	4,	'Ulu Tiram'),
(300,	4,	'Yong Peng'),
(301,	4,	'Nusajaya'),
(302,	4,	'Bukit Indah'),
(303,	4,	'Horizon Hills'),
(304,	4,	'Ledang East'),
(305,	4,	'Ledang Heights'),
(306,	4,	'Puteri Harbour'),
(307,	4,	'Kota Iskandar'),
(308,	5,	'Alor Setar'),
(309,	5,	'Ayer Hitam'),
(310,	5,	'Baling'),
(311,	5,	'Bandar Baharu'),
(312,	5,	'Bedong'),
(313,	5,	'Bukit Kayu Hitam'),
(314,	5,	'Guar Chempedak'),
(315,	5,	'Gurun'),
(316,	5,	'Jitra'),
(317,	5,	'Karangan'),
(318,	5,	'Kepala Batas'),
(319,	5,	'Kodiang'),
(320,	5,	'Kota Sarang Semut'),
(321,	5,	'Kota Setar'),
(322,	5,	'Kuala Kedah'),
(323,	5,	'Kuala Ketil'),
(324,	5,	'Kuala Muda'),
(325,	5,	'Kuala Nerang'),
(326,	5,	'Kubang Pasu'),
(327,	5,	'Kulim'),
(328,	5,	'Kupang'),
(329,	5,	'Langgar'),
(330,	5,	'Langkawi'),
(331,	5,	'Lunas'),
(332,	5,	'Merbok'),
(333,	5,	'Padang Serai'),
(334,	5,	'Padang Terap'),
(335,	5,	'Pendang'),
(336,	5,	'Pokok Sena'),
(337,	5,	'Pulau Langkawi'),
(338,	5,	'Serdang'),
(339,	5,	'Sik'),
(340,	5,	'Simpang Empat'),
(341,	5,	'Sungai Petani'),
(342,	5,	'University Utara'),
(343,	5,	'Yan'),
(344,	5,	'Changlun'),
(345,	5,	'Semeling'),
(346,	5,	'Sungai Lalang'),
(347,	5,	'Sungai Pasir'),
(348,	5,	'Tikam Batu'),
(349,	5,	'Tawar'),
(350,	5,	'Padang Meha'),
(351,	6,	'Ayer Lanas'),
(352,	6,	'Bachok'),
(353,	6,	'Cherang Ruku'),
(354,	6,	'Dabong'),
(355,	6,	'Gua Musang'),
(356,	6,	'Jeli'),
(357,	6,	'Kem Desa Pahlawan'),
(358,	6,	'Ketereh'),
(359,	6,	'Kota Bharu'),
(360,	6,	'Kuala Balah'),
(361,	6,	'Kuala Kerai'),
(362,	6,	'Kuala Krai'),
(363,	6,	'Machang'),
(364,	6,	'Melor'),
(365,	6,	'Pasir Mas'),
(366,	6,	'Pasir Puteh'),
(367,	6,	'Pulai Chondong'),
(368,	6,	'Rantau Panjang'),
(369,	6,	'Selising'),
(370,	6,	'Tanah Merah'),
(371,	6,	'Tawang'),
(372,	6,	'Temangan'),
(373,	6,	'Tumpat'),
(374,	6,	'Wakaf Baru'),
(375,	7,	'Alor Gajah'),
(376,	7,	'Asahan'),
(377,	7,	'Ayer Keroh'),
(378,	7,	'Bandar Hilir'),
(379,	7,	'Batu Berendam'),
(380,	7,	'Bemban'),
(381,	7,	'Bukit Beruang'),
(382,	7,	'Durian Tunggal'),
(383,	7,	'Jasin'),
(384,	7,	'Kuala Linggi'),
(385,	7,	'Kuala Sungai Baru'),
(386,	7,	'Lubok China'),
(387,	7,	'Masjid Tanah'),
(388,	7,	'Melaka Tengah'),
(389,	7,	'Merlimau'),
(390,	7,	'Selandar'),
(391,	7,	'Sungai Rambai'),
(392,	7,	'Sungai Udang'),
(393,	7,	'Tanjong Kling'),
(394,	7,	'Ujong Pasir'),
(395,	7,	'Bertam Malim'),
(396,	7,	'Bertam Ulu'),
(397,	7,	'Bukit Katil'),
(398,	7,	'Bukit Rambai'),
(399,	7,	'Bukit Serendek'),
(400,	7,	'Cheng'),
(401,	7,	'Klebang'),
(402,	7,	'Krubong'),
(403,	7,	'Taman Malim Jaya'),
(404,	7,	'Melaka Jaya'),
(405,	7,	'Taman Seri Paya Rumput'),
(406,	7,	'Pokok Mangga'),
(407,	7,	'Rembia'),
(408,	7,	'Taman Asean'),
(409,	7,	'Simpang Ampat'),
(410,	7,	'Melaka Raya'),
(411,	8,	'Bahau'),
(412,	8,	'Bandar Baru Serting'),
(413,	8,	'Batang Melaka'),
(414,	8,	'Batu Kikir'),
(415,	8,	'Gemas'),
(416,	8,	'Gemencheh'),
(417,	8,	'Jelebu'),
(418,	8,	'Jempol'),
(419,	8,	'Johol'),
(420,	8,	'Juasseh'),
(421,	8,	'Kota'),
(422,	8,	'Kuala Klawang'),
(423,	8,	'Kuala Pilah'),
(424,	8,	'Labu'),
(425,	8,	'Lenggeng'),
(426,	8,	'Linggi'),
(427,	8,	'Mantin'),
(428,	8,	'Nilai'),
(429,	8,	'Pasir Panjang'),
(430,	8,	'Pedas'),
(431,	8,	'Port Dickson'),
(432,	8,	'Rantau'),
(433,	8,	'Rembau'),
(434,	8,	'Rompin'),
(435,	8,	'Senawang'),
(436,	8,	'Seremban'),
(437,	8,	'Siliau'),
(438,	8,	'Simpang Durian'),
(439,	8,	'Simpang Pertang'),
(440,	8,	'Sri Menanti'),
(441,	8,	'Si Rusa'),
(442,	8,	'Tampin'),
(443,	8,	'Tanjong Ipoh'),
(444,	8,	'Mambau'),
(445,	8,	'Pajam'),
(446,	9,	'Balok'),
(447,	9,	'Bandar Pusat Jengka'),
(448,	9,	'Bandar Tun Abdul Razak'),
(449,	9,	'Benta'),
(450,	9,	'Bentong'),
(451,	9,	'Bera'),
(452,	9,	'Brinchang'),
(453,	9,	'Bukit Fraser'),
(454,	9,	'Cameron Highlands'),
(455,	9,	'Chenor'),
(456,	9,	'Daerah Rompin'),
(457,	9,	'Damak'),
(458,	9,	'Dong'),
(459,	9,	'Genting Highlands'),
(460,	9,	'Jerantut'),
(461,	9,	'Karak'),
(462,	9,	'Kuala Lipis'),
(463,	9,	'Kuala Rompin'),
(464,	9,	'Kuantan'),
(465,	9,	'Lanchang'),
(466,	9,	'Lipis'),
(467,	9,	'Lurah Bilut'),
(468,	9,	'Maran'),
(469,	9,	'Mengkarak'),
(470,	9,	'Mentakab'),
(471,	9,	'Muadzam Shah'),
(472,	9,	'Padang Tengku'),
(473,	9,	'Pekan'),
(474,	9,	'Raub'),
(475,	9,	'Ringlet'),
(476,	9,	'Rompin'),
(477,	9,	'Sega'),
(478,	9,	'Sungai Koyan'),
(479,	9,	'Sungai Lembing'),
(480,	9,	'Sungai Ruan'),
(481,	9,	'Tanah Rata'),
(482,	9,	'Temerloh'),
(483,	9,	'Triang'),
(484,	10,	'Ayer Tawar'),
(485,	10,	'Bagan Datoh'),
(486,	10,	'Bagan Serai'),
(487,	10,	'Batang Padang'),
(488,	10,	'Batu Gajah'),
(489,	10,	'Batu Kurau'),
(490,	10,	'Behrang Stesen'),
(491,	10,	'Beruas'),
(492,	10,	'Bidor'),
(493,	10,	'Bota'),
(494,	10,	'Changkat Jering'),
(495,	10,	'Changkat Keruing'),
(496,	10,	'Chemor'),
(497,	10,	'Chenderiang'),
(498,	10,	'Chenderong Balai'),
(499,	10,	'Chikus'),
(500,	10,	'Enggor'),
(501,	10,	'Gerik'),
(502,	10,	'Gopeng'),
(503,	10,	'Hilir Perak'),
(504,	10,	'Hulu Perak'),
(505,	10,	'Hutan Melintang'),
(506,	10,	'Intan'),
(507,	10,	'Ipoh'),
(508,	10,	'Jeram'),
(509,	10,	'Kampar'),
(510,	10,	'Kampong Gajah'),
(511,	10,	'Kampong Kepayang'),
(512,	10,	'Kamunting'),
(513,	10,	'Kerian'),
(514,	10,	'Kinta'),
(515,	10,	'Kuala Kangsar'),
(516,	10,	'Kuala Kurau'),
(517,	10,	'Kuala Sepetang'),
(518,	10,	'Lahat'),
(519,	10,	'Lambor Kanan'),
(520,	10,	'Langkap'),
(521,	10,	'Larut'),
(522,	10,	'Lenggong'),
(523,	10,	'Lumut'),
(524,	10,	'Malim Nawar'),
(525,	10,	'Mambang Diawan'),
(526,	10,	'Manong'),
(527,	10,	'Matang'),
(528,	10,	'Manjung'),
(529,	10,	'Menglembu'),
(530,	10,	'Padang Rengas'),
(531,	10,	'Pangkor'),
(532,	10,	'Pantai Remis'),
(533,	10,	'Parit'),
(534,	10,	'Parit Buntar'),
(535,	10,	'Pengkalan Hulu'),
(536,	10,	'Pusing'),
(537,	10,	'Rantau Panjang'),
(538,	10,	'Sauk'),
(539,	10,	'Selama'),
(540,	10,	'Selekoh'),
(541,	10,	'Selinsing'),
(542,	10,	'Semanggol'),
(543,	10,	'Seri Manjong'),
(544,	10,	'Simpang'),
(545,	10,	'Sitiawan'),
(546,	10,	'Slim River'),
(547,	10,	'Sungai Siput'),
(548,	10,	'Sungai Sumun'),
(549,	10,	'Sungkai'),
(550,	10,	'Taiping'),
(551,	10,	'Tanjong Piandang'),
(552,	10,	'Tanjong Rambutan'),
(553,	10,	'Tanjong Tualang'),
(554,	10,	'Tanjung Malim'),
(555,	10,	'Tapah'),
(556,	10,	'Teluk Intan'),
(557,	10,	'Temoh'),
(558,	10,	'TLDM Lumut'),
(559,	10,	'Trolak'),
(560,	10,	'Trong'),
(561,	10,	'Tronoh'),
(562,	10,	'Ulu Bernam'),
(563,	10,	'Ulu Kinta'),
(564,	10,	'Si Putih'),
(565,	10,	'Sri Iskandar'),
(566,	11,	'Arau'),
(567,	11,	'Kaki Bukit'),
(568,	11,	'Kangar'),
(569,	11,	'Kuala Perlis'),
(570,	11,	'Padang Besar'),
(571,	11,	'Pauh'),
(572,	11,	'Perlis'),
(573,	11,	'Simpang Ampat'),
(574,	12,	'Besut'),
(575,	12,	'Dungun'),
(576,	12,	'Hulu Terengganu'),
(577,	12,	'Kemaman'),
(578,	12,	'Kuala Terengganu'),
(579,	12,	'Marang'),
(580,	12,	'Setiu'),
(581,	12,	'Ajil'),
(582,	12,	'Al Muktafi Billah Shah'),
(583,	12,	'Bukit Besi'),
(584,	12,	'Cukai'),
(585,	12,	'Jerteh'),
(586,	12,	'Kemasek'),
(587,	12,	'Kerteh'),
(588,	12,	'Kijal'),
(589,	12,	'Kuala Berang'),
(590,	12,	'Paka'),
(591,	12,	'Permaisuri'),
(592,	12,	'Bukit Rakit'),
(593,	13,	'Beaufort'),
(594,	13,	'Beluran'),
(595,	13,	'Bongawan'),
(596,	13,	'Keningau'),
(597,	13,	'Kota Belud'),
(598,	13,	'Kota Kinabalu'),
(599,	13,	'Kota Kinabatangan'),
(600,	13,	'Kota Marudu'),
(601,	13,	'Kuala Penyu'),
(602,	13,	'Kudat'),
(603,	13,	'Kunak'),
(604,	13,	'Lahad Datu'),
(605,	13,	'Likas'),
(606,	13,	'Membakut'),
(607,	13,	'Menumbok'),
(608,	13,	'Nabawan'),
(609,	13,	'Pamol'),
(610,	13,	'Papar'),
(611,	13,	'Penampang'),
(612,	13,	'Pitas'),
(613,	13,	'Putatan'),
(614,	13,	'Ranau'),
(615,	13,	'Sandakan'),
(616,	13,	'Semporna'),
(617,	13,	'Sipitang'),
(618,	13,	'Tambunan'),
(619,	13,	'Tamparuli'),
(620,	13,	'Tawau'),
(621,	13,	'Tenom'),
(622,	13,	'Tuaran'),
(623,	13,	'Luyang'),
(624,	14,	'Asajaya'),
(625,	14,	'Balingian'),
(626,	14,	'Baram'),
(627,	14,	'Bau'),
(628,	14,	'Bekenu'),
(629,	14,	'Belaga'),
(630,	14,	'Belawai'),
(631,	14,	'Betong'),
(632,	14,	'Bintagor'),
(633,	14,	'Bintulu'),
(634,	14,	'Dalat'),
(635,	14,	'Daro'),
(636,	14,	'Debak'),
(637,	14,	'Engkilili'),
(638,	14,	'Julau'),
(639,	14,	'Kabong'),
(640,	14,	'Kanowit'),
(641,	14,	'Kapit'),
(642,	14,	'Kota Samarahan'),
(643,	14,	'Kuching'),
(644,	14,	'Lawas'),
(645,	14,	'Limbang'),
(646,	14,	'Lingga'),
(647,	14,	'Long Lama'),
(648,	14,	'Lubok Antu'),
(649,	14,	'Lundu'),
(650,	14,	'Lutong'),
(651,	14,	'Maradong'),
(652,	14,	'Marudi'),
(653,	14,	'Matu'),
(654,	14,	'Miri'),
(655,	14,	'Mukah'),
(656,	14,	'Nanga Medamit'),
(657,	14,	'Niah'),
(658,	14,	'Pusa'),
(659,	14,	'Roban'),
(660,	14,	'Saratok'),
(661,	14,	'Sarikei'),
(662,	14,	'Sebauh'),
(663,	14,	'Sebuyau'),
(664,	14,	'Serian'),
(665,	14,	'Sibu'),
(666,	14,	'Simunjan'),
(667,	14,	'Song'),
(668,	14,	'Spaoh'),
(669,	14,	'Sri Aman'),
(670,	14,	'Sundar'),
(671,	14,	'Tanjung Kidurong'),
(672,	14,	'Tatau'),
(673,	15,	'Labuan'),
(674,	16,	'Putrajaya'),
(675,	16,	'Cyberjaya');

CREATE TABLE `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `district_id` int(10) unsigned NOT NULL,
  `address` varchar(255) NOT NULL,
  `salary` decimal(5,2) unsigned NOT NULL,
  `salary_unit` int(2) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `district_id` (`district_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `jobs_ibfk_2` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jobs_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `states` (`id`, `name`) VALUES
(1,	'Kuala Lumpur'),
(2,	'Selangor'),
(3,	'Penang'),
(4,	'Johor'),
(5,	'Kedah'),
(6,	'Kelantan'),
(7,	'Melaka'),
(8,	'Negeri Sembilan'),
(9,	'Pahang'),
(10,	'Perak'),
(11,	'Perlis'),
(12,	'Terengganu'),
(13,	'Sabah'),
(14,	'Sarawak'),
(15,	'Labuan'),
(16,	'Putrajaya');

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `last_login` int(10) unsigned DEFAULT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2012-12-26 17:57:56