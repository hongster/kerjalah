<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	'user_id' => array(
		'not_empty' => 'User ID must not be empty.',
	),
	'company' => array(
		'max_length' => 'Company name must not exceed 255 characters.',
	),
	'title' => array(
		'not_empty' => 'Please specify a title for this job post.',
		'max_length' => 'Title must not exceed 255 characters.',
	),
	'description' => array(
		'not_empty' => 'Please specify job description for this job post.',
	),
	'district_id' => array(
		'not_empty' => 'District ID must not be empty.',
		'default' => 'Invalid District ID.',
	),
	'address' => array(
		'not_empty' => 'Please specify address of workplace.',
		'max_length' => 'Address must not exceed 255 characters.',
	),
	'salary' => array(
		'not_empty' => 'Please specify salary.',
		'range' => 'Salary value must not exceed RM 1000.',
	),
	'salary_unit' => array(
		'not_empty' => 'Salary unit must not be empty.',
		'default' => 'Invalid salary unit.',
	),
	'start_date' => array(
		'not_empty' => 'Please specify the working date.',
		'default' => 'Invalid start date.',
	),
	'end_date' => array(
		'not_empty' => 'Please specify the last date of work.',
		'date' => 'Invalid end date.',
		'valid_end_date' => 'End date must be after start date.',
	),
	'start_time' => array(
		'default' => 'Invalid start time.',
	),
	'end_time' => array(
		'start_before_end' => 'End time must be after start time.',
		'default' => 'Invalid end time.',
	),
);