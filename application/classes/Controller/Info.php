<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Used internally to provide information (mostly used in Views).
 */
class Controller_Info extends Controller {
	/**
	 * Get list of states and its districts.
	 */
	public function action_areas()
	{
		$data = array();
		$districts = ORM::factory('District')->all_districts();

		foreach ($districts as $district)
		{
			if ( ! array_key_exists($district->state_id, $data))
			{
				$data[$district->state_id] = array(
					'name' => $district->state->name,
					'districts' => array(),
				);
			}
			
			$data[$district->state_id]['districts'][$district->id] = $district->name;
		}

		return $this->response->body(json_encode($data));
	}
}