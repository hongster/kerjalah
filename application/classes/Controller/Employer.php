<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Employer extends Controller_Web {

	protected $_no_login = array();

	public function action_index()
	{
		$user = Authen::instance()->user();
		$values = array_merge(array(), $_GET); // Search parameters
		$limit = 20;

		$paginate = My_Paginate::factory();
		$paginate->page_count( (int) ceil($user->search_jobs_count($values) / (float) $limit));
		$this->view->paginate = $paginate;

		$page = $paginate->current_page();
		$offset = ($page - 1) * $limit; 
		$this->view->jobs = $user->search_jobs($values, $limit, $offset);

		$this->template->title = 'My Job Posts';
	}
}