<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Job extends Controller_Web {

	protected $_no_login = array('index', 'info', 'from_company');

	public function action_index()
	{
		$model = ORM::factory('Job');

		$values = array_merge(array(), $_GET); // Search parameters
		$limit = 20;

		$paginate = My_Paginate::factory('job_search');
		$paginate->page_count( (int) ceil($model->search_count($values) / (float) $limit));

		$page = $paginate->current_page();
		$offset = ($page - 1) * $limit; 
		$jobs = $model->search($values, $limit, $offset);

		$this->template->title = $this->_query_title($values);
		$this->template->description = $this->template->title;
		$this->view->set(array(
			'title' => $this->template->title,
			'values' => $values,
			'jobs' => $jobs,
			'paginate' => $paginate,
		));
	}

	/**
	 * Generate suitable page title based on values in search query.
	 *
	 * @param array $query Seach query
	 * @return string
	 */
	public function _query_title($query = array())
	{
		$result = array();
		
		// Title
		if ($title = Arr::get($query, 'title'))
		{
			$result[] = '"'.$title.'"';
		}

		(count($result) == 0) AND $result[] = 'Job Listing';

		// Area
		if ($district_id = Arr::get($query, 'district_id'))
		{
			$district = ORM::factory('District', $district_id);
			$result[] = 'in '.$district->name.', '.$district->state->name;
		}
		elseif ($state_id = Arr::get($query, 'state_id'))
		{
			$state = ORM::factory('State', $state_id);
			$result[] = 'in '.$state->name;
		}

		// Date
		if ($date = Arr::get($query, 'date'))
		{
			$result[] = 'on '.$date;
		}

		// Company
		if ($company = Arr::get($query, 'company'))
		{
			$result[] = 'by '.$company;
		}

		return implode(' ', $result);
	}

	/**
	 * @param int $id Optional. Job post to clone from.
	 */
	public function action_post()
	{				
		if ($_POST)
		{			
			try {
				$values = array_merge($_POST, array(
					'user_id' => Authen::instance()->user()->id,
					'updated' => time(),
				));
				$job = ORM::factory('Job')
					->values($values)
					->create();
				
				$this->_flash_success('Job post has been saved successfully.');
				return $this->redirect(Template::job_url($job));
			} catch (ORM_Validation_Exception $e) {
				$errors = $e->errors('');
			}
		}
		elseif ($job_id = $this->request->param('id', FALSE))
		{
			// Clone job post that belongs to this user
			$job = ORM::factory('Job', $job_id);
			if ($job->user_id == Authen::instance()->user()->id)
			{
				$values = $job->as_array();
				unset($values['start_date']);
				unset($values['end_date']);
			}
		}

		$this->view
			->bind('values', $values)
			->bind('errors', $errors);

		$this->template->title = 'Post New Job';
		$this->view->cancel_url = URL::site('employer/index', TRUE);
	}

	/**
	 * @param int $id Job ID.
	 */
	public function action_update()
	{
		$user_id = Authen::instance()->user()->id;
		// Check permission
		$job = ORM::factory('Job', $this->request->param('id'));
		if ($job->user_id != $user_id)
		{
			$this->_flash_error('You are not allowed to edit this job post.');
			return $this->redirect($this->request->referrer());
		}
				
		if ($_POST)
		{	
			$values = $_POST;		
			try {
				$values['user_id'] = $user_id;
				
				$job = $job
					->values($values)
					->save();
				
				$this->_flash_success('Job post has been updated successfully.');
				return $this->redirect(Template::job_url($job));	
			} catch (ORM_Validation_Exception $e) {
				$errors = $e->errors('');
			}
		}
		else
		{
			$values = $job->as_array();
			$values['state_id'] = $job->district->state_id;
		}

		$this->view
			->set('values', $values)
			->bind('errors', $errors);

		$this->template->title = 'Update Job Post';
		$this->view->cancel_url = Template::job_url($job);
	}

	/**
	 * @param int $id Job ID.
	 */
	public function action_delete()
	{
		$user_id = Authen::instance()->user()->id;
		// Check permission
		$job = ORM::factory('Job', $this->request->param('id'));
		if ($job->user_id != $user_id)
		{
			$this->_flash_error('You are not allowed to delete this job post.');
			return $this->redirect($this->request->referrer());
		}

		$job->delete();
		$this->_flash_info('Job post has been deleted.');
		$this->redirect('');
	}

	/**
	 * Display detail job information.
	 * 
	 * @param int $id Job ID.
	 */
	public function action_info()
	{
		$job = ORM::factory('Job', $this->request->param('id'));
		if ( ! $job->loaded())
		{
			$this->_flash_error('Job information is not available.');
			return $this->redirect('');
		}

		$this->template->title = $job->title;
		
		$excerpt = mb_substr($job->description, 0, 200);
		(mb_strlen($excerpt) >= 200) AND $excerpt .= '...';
		$this->template->description = '['.$job->district->name.', '.$job->district->state->name.'] '.$excerpt;

		$this->template->canonical_url = Template::job_url($job);

		$this->view->job = $job;
		$this->view->related_jobs = $job->related_jobs();
	}

	/**
	 * List all jobs from a company.
	 * 
	 * @param string $company Company name.
	 */
	public function action_from_company()
	{
		$model = ORM::factory('Job');

		$company = $this->request->param('company', '');
		$limit = 20;

		$paginate = My_Paginate::factory('jobs_from_company');
		$paginate->page_count( (int) ceil($model->from_company_count($company) / (float) $limit));

		$page = $paginate->current_page();
		$offset = ($page - 1) * $limit; 
		$jobs = $model->from_company($company, $limit, $offset);

		$this->view->set(array(
			'company' => $company,
			'jobs' => $jobs,
			'paginate' => $paginate,
		));

		$this->template->title = 'Upcoming Jobs From '.$this->request->param('company');
		$this->template->description = 'Upcoming jobs from '.$this->request->param('company');
	}
}