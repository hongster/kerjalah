<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Web {
	/* Session key for storing URL to redirect after successful login. */
	const LOGIN_REDIRECTION_KEY = 'auth.login_redirection';

	protected $_no_login = array('index', 'logout');

	public function before()
	{
		if ($this->request->action() == 'index')
		{
			$this->auto_render = TRUE;
		}
		else
		{
			$this->auto_render = FALSE;
		}

		parent::before();
	}

	/**
	 * Entry point for login.
	 */
	public function action_index()
	{
		// AJAX call after successful authentication by provider
		if ($this->request->is_ajax())
		{
			$this->auto_render = FALSE;
			if ($this->_login_user())
			{
				return $this->response
					->headers('Content-Type', 'text/json')
					->body(json_encode(array(
						'redirectUrl' => URL::site(
							Session::instance()->get_once(static::LOGIN_REDIRECTION_KEY, ''),
							TRUE
						),
					)));
			}
			else
			{
				return $this->response
					->headers('Content-Type', 'text/json')
					->body(json_encode(array(
						'error' => 'Login failed.',
					)));
			}
		}

		// Not authenticated yet
		$this->template->title = 'Login';
	}

	/**
	 * User has already authenticated successfully. Use $_POST['token']
	 * to retrieve user info from Jainrain. Create account, if this is new user.
	 * Login user.
	 *
	 * @param bool
	 */
	private function _login_user()
	{
		$profile = $this->_get_user_profile($this->request->post('token'));
		if ( ! $profile)
			return FALSE;

		$is_success = Authen::instance()->login(
			$profile['displayName'],
			$profile['email'],
			$profile['providerName'],
			$profile['identifier']
		);

		return $is_success;
	}

	/**
	 * Call the Janrain auth_info API to retieve authenticated user's profile.
	 * Requires api_key to be set in janrain.php config file.
	 *
	 * @param string $token The token parameter received at your token_url.
	 * @return array|NULL auth_info response. NULL if unable to retireve user info.
	 */
	private function _get_user_profile($token)
	{
		$post_data = array(
			'token' => $token,
			'apiKey' => Kohana::$config->load('janrain')->get('api_key'),
		);
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, 'https://rpxnow.com/api/v2/auth_info');
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FAILONERROR, true);
		$result = curl_exec($curl);
		
		if ($result == FALSE)
		{
			Kohana::$log->add(Log::ERROR, "Controller_Auth::_get_user_profile($token) CURL failed. ".curl_error($curl))->write();
			return NULL;
		}
		curl_close($curl);

		$auth_info = json_decode($result, TRUE);
		if ($auth_info['stat'] != 'ok')
		{
			Kohana::$log->add(Log::ERROR, "Controller_Auth::_get_user_profile($token) Invalid auth_info. ".$result)->write();
			return NULL;
		}

		return $auth_info['profile'];
	}

	public function action_logout()
	{
		Authen::instance()->logout();
		return $this->redirect('');
	}
}