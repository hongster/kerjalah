<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Controller for automatic template.
 */
class Controller_Web extends Controller_Template {
	const ERROR_MESSAGE = 'web.error_message';
	const SUCCESS_MESSAGE = 'web.success_message';
	const INFO_MESSAGE = 'web.info_message';

	/** @var Actions that do not require login. */
	protected $_no_login = array();

	public function __get($name)
	{
		// Lazy initialization of view
		if ($name == 'view')
		{
			if ( ! isset($this->view))
			{
				$this->_init_view();
			}
			
			return $this->view;
		}
	}

	public function before() 
	{
		if (( ! in_array($this->request->action(), $this->_no_login))
		&& ( ! Authen::instance()->logged_in()))
		{
			Session::instance()->set(Controller_Auth::LOGIN_REDIRECTION_KEY, $this->request->uri());
			return $this->redirect(Route::get('login')->uri());
		}
		
		parent::before();
	}
	
	public function after() 
	{	
		if ($this->auto_render === TRUE)
		{
			if ( ! isset($this->view)) 
			{
				$this->_init_view();
			}
			
			$this->template->content = $this->view;

			// Set title
			if ( ! isset($this->template->title))
			{
				$this->template->title = 'Part Time Job Listing In Malaysia';
			}

			// Set description
			$this->template->description = isset($this->template->description)
				? $this->template->description
				: 'Part time jobs listing in Malaysia. Employers advertise part time jobs. Job seekers contact employers for part time jobs.';
		}
		
		parent::after();
	}

	/**
	 * Load view based on action name.
	 * View is assumed in /application/views/(<directory>/)<controller_name>/<action>.php
	 */
	private function _init_view()
	{
		$this->view = View::factory(
			$this->request->directory().DIRECTORY_SEPARATOR
			.$this->request->controller().DIRECTORY_SEPARATOR
			.$this->request->action()
		);
	}

	/**
	 * Flash success message in template.
	 *
	 * @param string $message
	 */
	protected function _flash_success($message)
	{
		Session::instance()->set(static::SUCCESS_MESSAGE, $message);
	}

	/**
	 * Flash error message in template.
	 *
	 * @param string $message
	 */
	protected function _flash_error($message)
	{
		Session::instance()->set(static::ERROR_MESSAGE, $message);
	}

	/**
	 * Flash informational message in template.
	 *
	 * @param string $message
	 */
	protected function _flash_info($message)
	{
		Session::instance()->set(static::INFO_MESSAGE, $message);
	}
}