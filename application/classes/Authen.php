<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Custom authentication class for Google login.
 */
class Authen {
	const USER_ID_KEY = 'authen.user_id';

	/** @var Singleton.  */
	private static $_instance = null;

	/** @var Model_User Loaded on successful login. */
	private $_user = NULL;

	/**
	 * Cannot be instantiated directly. This is a Singleton class.
	 */
	private function __construct()
	{
		// Auto load logged in user model instance
		$user_id = Session::instance()->get(static::USER_ID_KEY, NULL);
		
		if ( ! $user_id)
			return;

		if ( ! $this->force_login($user_id))
		{
			Kohana::$log->add(Log::ERROR, "Authen::__construct unable to load user $user_id.")->write();
			return;
		}
	}

	public static function instance()
	{
		if (is_null(static::$_instance))
		{
			static::$_instance = new static;
		}

		return static::$_instance;
	}

	/**
	 * If this is a new user, account will be created. Login the user.
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $google_uid
	 * @return bool
	 */
	public function login($name, $email, $provider, $identifier)
	{
		// Check if this is new user
		$authentication = ORM::factory('Authentication')
			->where('provider', '=', $provider)
			->where('identifier', '=', $identifier)
			->find();
		$user = $authentication->user;

		if ( ! $authentication->loaded())
		{
			// New user
			$user->clear();
			$user->name = $name;
			$user->email = $email;
			try {
				if ( ! $user->save())
				{
					Kohana::$log
						->add(Log::ERROR, "Authen::login($name, $email, $provider, $identifier) failed to create User record.")
						->write();
					return FALSE;
				}
			} 
			catch (Database_Exception $e) 
			{
				// Probably duplicated email address in "users" table. 
				Kohana::$log
					->add(Log::ERROR, "Authen::login($name, $email, $provider, $identifier) ".$e->getMessage())
					->write();
				return FALSE;
			}

			$authentication->clear();
			$authentication->provider = $provider;
			$authentication->identifier = $identifier;
			$authentication->user_id = $user->id;
			if ( ! $authentication->save())
			{
				Kohana::$log
					->add(Log::ERROR, "Authen::login($name, $email, $provider, $identifier) failed to create Authentication record.")
					->write();
				return FALSE;
			}
		}

		return $this->force_login($user);
	}

	public function logout()
	{
		$this->_user = NULL;
		$session = Session::instance();
		$session->delete(static::USER_ID_KEY);
	}

	/**
	 * Login an user with Google credentials.
	 * 
	 * @param int|Model_User
	 * @return bool
	 */
	public function force_login($user)
	{
		is_numeric($user) AND $user = ORM::factory('User', $user);

		if ( ! $user->loaded())
			return false;

		$this->_user = $user;

		// Set a 5 min threshold, so DB is not hit every time Authen is instantiated.
		if (time() - $this->_user->last_login > 300)
		{
			$this->_user->login(); // Update login time
		}
		
		$session = Session::instance();
		$session->regenerate();
		$session->set(static::USER_ID_KEY, $this->_user->id);

		return true;
	}

	/**
	 * Return logged in user's model instance.
	 * 
	 * @return Model_User. NULL if user has not logged in.
	 */
	public function user()
	{
		return $this->_user;
	}

	/**
	 * @return bool
	 */
	public function logged_in()
	{
		return ! is_null($this->_user);
	}
}