<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Helper functions for template.
 */
class Template {
	/**
	 * Generate an URL that points to the job info page.
	 * 
	 * @param Model_Job
	 * @return string URL pointing to job info page.
	 */
	public static function job_url($job)
	{
		return URL::site(
			Route::get('job_info')->uri(array('id' => $job->id, 'title' => URL::title($job->title))),
			TRUE
		);
	}
}