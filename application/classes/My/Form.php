<?php defined('SYSPATH') or die('No direct script access.');

class My_Form extends Form {
	/**
	 * @param string $name Input name.
	 * @param string $data Data array containing the input value.
	 * @param array $attributes HTML attributes.
	 * @return string
	 */
	public static function input($name, $data = array(), array $attributes = array())
	{
		$value = Arr::get($data, $name, NULL);
		$attributes = array_merge(array('id' => $name), $attributes);

		return parent::input($name, $value, $attributes);
	}

	/**
	 * @param string $name Input name.
	 * @param string $data Data array containing the input value.
	 * @param array $attributes HTML attributes.
	 * @return string
	 */
	public static function hidden($name, $data = array(), array $attributes = array())
	{
		$value = Arr::get($data, $name, NULL);
		$attributes = array_merge(array('id' => $name), $attributes);

		return parent::hidden($name, $value, $attributes);
	}

	/**
	 * @param string $name Textarea name.
	 * @param string $data Data array containing the input value.
	 * @param array $attributes HTML attributes.
	 * @param boolean $double_encode  encode existing HTML characters
	 * @return string
	 */
	public static function textarea($name, $data = array(), array $attributes = array(), $double_encode = TRUE)
	{
		$value = Arr::get($data, $name, NULL);
		$attributes = array_merge(array('id' => $name), $attributes);

		return parent::textarea($name, $value, $attributes, $double_encode);
	}

	/**
	 * @param string $name Input name
	 * @param array $options Available options
	 * @param mixed $data Data array containing selected option string, or an array of selected options
	 * @param array $attributes HTML attributes
	 * @return string
	 */
	public static function select($name, array $options = NULL, $data = array(), array $attributes = array())
	{
		$selected = Arr::get($data, $name, NULL);
		$attributes = array_merge(array('id' => $name), $attributes);

		return parent::select($name, $options, $selected, $attributes);
	}

	/**
	 * Output inline validation error message.
	 *
	 * @param string $field
	 * @param array $errors
	 * @return string
	 */
	public static function error($field, $errors)
	{
		if ( ! ($error = Arr::path($errors, $field, FALSE)))
			return '';

		return '<span class="text-error">'.$error.'</span>';
	}
}