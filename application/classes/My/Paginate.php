<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Helper class for generating pagination in view.
 */
class My_Paginate {
	/** @var int Total number of pages. */
	private $_count = 0;

	/** @var int Number of page links to display. */
	private $_modulus = 9;

	/** @var array Maintain multiple instances of My_Paginate. */
	private static $_instances = array();

	/**
	 * Return an instance of My_Paginate. Multiple instances will be maintained,
	 * and they are retireved by an unique identifier, $key.
	 * 
	 * @param string $key Identifier for this instance.
	 * @return My_Paginate
	 */
	public static function factory($key = NULL)
	{
		is_null($key) AND $key = 'My_Paginate'; // Default key.

		if ( ! isset(static::$_instances[$key]))
		{
			static::$_instances[$key] = new static;	
		}

		return static::$_instances[$key];
	}

	/**
	 * Render the pagination view.
	 * - My_Property::factory()->render('template/paginate');
	 * - My_Property::factory()->render(View::factory('template/paginate'));
	 * - My_Property::factory()->render();
	 * 
	 * @param mixed $view View file for generating pagination view.
	 * @return string Rendered pagination view.
	 */
	public function render($view = NULL)
	{
		if (is_null($view))
		{
			$view = View::factory('Template/paginate'); // Default
		}
		elseif ( ! ($view instanceof View))
		{
			$view = View::factory($view); 
		}

		return $view->set('paginate', $this)->render();
	}

	/**
	 * Getter/setter total number of pages.
	 * 
	 * @param int $count
	 * @return mixed
	 */
	public function page_count($count = NULL)
	{
		if (is_null($count))
			return $this->_count;

		$this->_count = $count;
		return $this;
	}

	/**
	 * Current page number, starting from 1.
	 * 
	 * @return int
	 */
	public function current_page()
	{
		return (int) Arr::get(Request::current()->query(), 'page', 1);
	}

	/**
	 * Getter/setter. Number of page links to display.
	 * 
	 * @param int $modulus Default 9.
	 * @return mixed
	 */
	public function modulus($modulus = NULL)
	{
		if (is_null($modulus))
			return $this->_modulus;

		$this->_modulus = $modulus;
		return $this;
	}

	/**
	 * Return starting page number, based on modulus.
	 * 
	 * @return int
	 */
	public function start_page()
	{
		$half_modulus = (int) ($this->_modulus / 2);
		$start_page = min($this->current_page() - $half_modulus, $this->page_count() - $this->_modulus + 1);
		return max(1, $start_page);
	}

	/**
	 * Return ending page number, based on modulus.
	 * 
	 * @return int
	 */
	public function end_page()
	{
		$half_modulus = (int) ($this->_modulus / 2);
		$end_page = max($this->current_page() + $half_modulus, $this->_modulus);
		return min($this->page_count(), $end_page);
	}

	/**
	 * Generate URL for a selected page.
	 * 
	 * @param int $page Page number. Default to current page.
	 * @return string URL.
	 */
	public function page_url($page = NULL)
	{
		is_null($page) AND $page = $this->current_page();

		$request = Request::current();
		
		$query = $request->query();
		$query['page'] = $page;

		return $request->uri().URL::query($query);
	}
}