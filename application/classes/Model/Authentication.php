<?php defined('SYSPATH') OR die('No direct script access.');

class Model_Authentication extends ORM {
	protected $_created_column = array('column' => 'created', 'format' => true);

	protected $_belongs_to = array(
		'user' => array(),
	);
}