<?php defined('SYSPATH') OR die('No direct script access.');

class Model_User extends ORM {
	protected $_updated_column = array('column' => 'updated', 'format' => true);

	protected $_created_column = array('column' => 'created', 'format' => true);

	protected $_has_many = array(
		'jobs' => array(),
	);

	protected $_has_one = array(
		'authentication' => array(),
	);

	/**
	 * Update login time.
	 */
	public function login()
	{
		if ( ! $this->loaded())
			throw new Kohana_Exception('Method login() cannot be called on unloaded object.');

		$this->last_login = time();
		return $this->save();
	}

	/**
	 * Search jobs belonging to this user.
	 * 
	 * @param $conditions Search parameters
	 *	- title
	 * @param int $limit
	 * @param int $offset
	 * @return Database_Result
	 */
	public function search_jobs($conditions = array(), $limit = NULL, $offset = NULL)
	{
		if ( ! $this->loaded())
			throw new Kohana_Exception('Method all_jobs() cannot be called on unloaded object.');

		$jobs = $this->jobs;
		is_null($limit) OR $jobs->limit($limit);
		is_null($offset) OR $jobs->offset($offset);

		if ($title = Arr::get($conditions, 'title', FALSE))
		{
			$jobs->where('title', 'LIKE', "%$title%");
		}

		return $jobs
			->order_by('updated', 'DESC')
			->order_by('created', 'DESC')
			->find_all();
	}

	/**
	 * Count number of records in result. For pagination.
	 * 
	 * @param $conditions Refer to search_jobs.
	 * @param int $limit
	 * @param int $offset
	 * @return Database_Result
	 */
	public function search_jobs_count($conditions = array())
	{
		if ( ! $this->loaded())
			throw new Kohana_Exception('Method all_jobs() cannot be called on unloaded object.');

		$jobs = DB::select(array(DB::expr('COUNT(id)'), 'search_jobs_count'))
			->from(ORM::factory('Job')->table_name())
			->where('user_id', '=', $this->id);
		
		if ($title = Arr::get($conditions, 'title', FALSE))
		{
			$jobs->where('title', 'LIKE', "%$title%");
		}

		return $jobs
			->execute()
			->get('search_jobs_count');
	}
}