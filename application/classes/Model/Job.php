<?php defined('SYSPATH') OR die('No direct script access.');

class Model_Job extends ORM {
	protected $_updated_column = array('column' => 'updated', 'format' => true);

	protected $_created_column = array('column' => 'created', 'format' => true);

	protected $_belongs_to = array(
		'user' => array(),
		'district' => array(),
	);

	public function rules()
	{
		return array(
			'user_id' => array(
				array('not_empty'),
			),
			'company' => array(
				array('max_length', array(':value', 255)),
			),
			'title' => array(
				array('not_empty'),
				array('max_length', array(':value', 255)),
			),
			'description' => array(
				array('not_empty'),
			),
			'district_id' => array(
				array('not_empty'),
				array(array($this, 'valid_district_id')),
			),
			'address' => array(
				array('not_empty'),
				array('max_length', array(':value', 255)),
			),
			'salary' => array(
				array('not_empty'),
				array('range', array(':value', 1.0, 999.99)),
			),
			'salary_unit' => array(
				array('not_empty'),
				array('regex', array(':value', '/(1|24)/')),
			),
			'start_date' => array(
				array('not_empty'),
				array('date'),
			),
			'end_date' => array(
				array('not_empty'),
				array('date'),
				array(array($this, 'valid_end_date'), array(':value', ':model')),
			),
			'start_time' => array(
				array('regex', array(':value', '/[0-2][0-9]:[03][0-9]/')),
			),
			'end_time' => array(
				array('regex', array(':value', '/[0-2][0-9]:[03][0-9]/')),
				array(array($this, 'start_before_end'), array(':model')),
			),
		);
	}

	public function filters()
	{
		return array(
			'start_time' => array(
				array(function($value) {
					return ($value == '') ? NULL : $value;
				}),
			),
			'end_time' => array(
				array(function($value) {
					return ($value == '') ? NULL : $value;
				}),
			),
		);
	}

	/**
	 * $conditions:
	 *	- date 'YYYY-MM-DD'
	 *	- state_id
	 *	- district_id
	 *	- title
	 *	- company
	 * 
	 * @param array conditions
	 * @param int $limit
	 * @param int $offset
	 * @return Database_Result
	 */
	public function search($conditions = array(), $limit = 20, $offset = 0)
	{
		$this->reset();
		$limit = min(100, $limit);

		// Convert $conditions to queries.
		$this->_build_search_query($conditions, $this);

		return $this
			->order_by('start_date')
			->order_by('end_date')
			->order_by('updated')
			->limit($limit)
			->offset($offset)
			->find_all();
	}

	/**
	 * Returns number of records in search result.
	 * 
	 * @param array conditions Refer to search().
	 * @return int Number of records in search result.
	 */
	public function search_count($conditions = array())
	{
		return $this->_build_search_query($conditions, DB::select(array(DB::expr('COUNT(id)'), 'search_count'))) 
			->from($this->_table_name)
			->execute()
			->get('search_count');
	}

	/**
	 * @param array $conditions Refer to search().
	 * @param mixed $query_builder
	 * @return mixed The $query_builder
	 */
	private function _build_search_query($conditions, $query_builder)
	{
		if ($date = Arr::get($conditions, 'date', FALSE))
		{
			$query_builder->where('start_date', '<=', $date)
				->where('end_date', '>=', $date);
		}
		else
		{
			// Load upcoming jobs if actual date is not specified.
			$query_builder->where('start_date', '>', date('Y-m-d'));
		}

		if ($district_id = Arr::get($conditions, 'district_id', FALSE))
		{
			$query_builder->where('district_id', '=', $district_id);
		}
		elseif ($state_id = Arr::get($conditions, 'state_id', FALSE))
		{
			$query_builder->where('district_id', 'IN', $this->_get_districts($state_id));	
		}

		if ($title = Arr::get($conditions, 'title', FALSE))
		{
			$query_builder->where('title', 'LIKE', "%$title%");
		}

		if ($company = Arr::get($conditions, 'company', FALSE))
		{
			$query_builder->where('company', 'LIKE', "%$company%");
		}

		return $query_builder;
	}

	/**
	 * Search for related jobs. Based on district and date.
	 * Return max 5 results.
	 *
	 * @return Database_Result
	 */
	public function related_jobs()
	{
		if ( ! $this->loaded())
			throw new Kohana_Exception('Method related_jobs() cannot be called on unloaded object.');

		return ORM::factory($this->object_name())
			->where('district_id', '=', $this->district->id)
			->where('start_date', '>', date('Y-m-d'))
			->where('id', '!=', $this->id)
			->order_by('start_date', 'ASC')
			->limit(5)
			->find_all();
	}

	/**
	 * Retrieve list of all district
	 *
	 * @param int $state_id
	 * @return array List of all district IDs.
	 */
	private function _get_districts($state_id)
	{
		$districts = ORM::factory('District')
			->where('state_id', '=', $state_id)
			->find_all();

		$result = array();
		foreach ($districts as $district)
		{
			$result[] = $district->id;
		}

		return $result;
	}

	/**
	 * Retrieve all jobs from this company.
	 * 
	 * @param string $company
	 * @param int $limit Default 20.
	 * @param int $offset Default 0.
	 * @return Database_Result
	 */
	public function from_company($company, $limit = 20, $offset= 0)
	{
		return $this->search(array('company' => $company), $limit, $offset);
	}

	/**
	 * Retrieve all jobs from this company.
	 * 
	 * @param string $company
	 * @return Database_Result
	 */
	public function from_company_count($company)
	{
		return $this->search_count(array('company' => $company));
	}

	/**
	 * Determine if end_date is >= start_date.
	 *
	 * @param string $value end_date value.
	 * @param Model_Job
	 * @return bool
	 */
	public function valid_end_date($value, $model)
	{
		return strtotime($value) >= strtotime($model->start_date);
	}

	/**
	 * @param int $district_id
	 * @return bool
	 */
	public function valid_district_id($district_id)
	{
		return ORM::factory('District', $district_id)->loaded();
	}

	/**
	 * Check that the end time is after start time, not before.
	 * 
	 * @param Model_Job $model
	 * @return bool
	 */
	public function start_before_end($model)
	{
		// Check only if both start_time and end_time are set
		if (( ! isset($model->start_time)) || ( ! isset($model->start_time)))
			return true;
		
		return $model->start_time < $model->end_time;
	}
}