<?php defined('SYSPATH') OR die('No direct script access.');

class Model_District extends ORM {
	protected $_belongs_to = array(
		'state' => array(),
	);

	protected $_has_many = array(
		'jobs' => array(),
	);

	/**
	 * Return all districts, sorted by state_id, name.
	 *
	 * @return Model_District
	 */
	public function all_districts()
	{
		return $this->order_by('state_id', 'ASC')
			->order_by('name', 'ASC')
			->find_all();
	}
}